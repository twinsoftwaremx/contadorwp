﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ContadorWP.Resources;
using ContadorWP.Model;
using Windows.UI;
using System.Windows.Media;

namespace ContadorWP
{
	public partial class CounterDetailsPage : PhoneApplicationPage
	{
		private Counter counter;

		private ApplicationBarMenuItem deleteMenuItem;

		public CounterDetailsPage()
		{
			InitializeComponent();

			Loaded += CounterDetailsPage_Loaded;

			BuildApplicationBar();
		}

		void CounterDetailsPage_Loaded(object sender, RoutedEventArgs e)
		{
			int ID = 0;

			if (NavigationContext.QueryString.Count > 0 && NavigationContext.QueryString["ID"] != null)
			{
				int.TryParse(NavigationContext.QueryString["ID"], out ID);
			}

			if (ID > 0)
			{
				var repository = new CountersRepository();
				counter = repository.GetCounterByID(ID);

				var colorItem = new ColorItem();
				colorItem.Text = counter.Color;

				NameText.Text = counter.Name;
				CountText.Text = counter.Count.ToString();
				AllowNegativesCheckBox.IsChecked = counter.AllowNegatives;
				SelectColorButton.Background = new SolidColorBrush(ColorItem.ConvertColor(counter.Color));
			}
			else
			{
				counter = new Counter();
			}

			ColorItem item = (App.Current as App).CurrentColorItem;
			if (item != null)
			{
				counter.Color = item.Text;

				SelectColorButton.Background = new SolidColorBrush(item.Color);

				(App.Current as App).CurrentColorItem = null;
			}

			if (counter.ID <= 0)
			{
				deleteMenuItem.IsEnabled = false;
			}
		}

		private void BuildApplicationBar()
		{
			ApplicationBar = new ApplicationBar();

			var saveButton = new ApplicationBarIconButton()
			{
				IconUri = new Uri("/Assets/Icons/Check.png", UriKind.Relative),
				Text = AppResources.CounterDetailsSave
			};
			saveButton.Click += saveButton_Click;
			ApplicationBar.Buttons.Add(saveButton);

			var cancelButton = new ApplicationBarIconButton()
			{
				IconUri = new Uri("/Assets/Icons/Close.png", UriKind.Relative),
				Text = AppResources.CounterDetailsCancel
			};
			cancelButton.Click += cancelButton_Click;
			ApplicationBar.Buttons.Add(cancelButton);

			deleteMenuItem = new ApplicationBarMenuItem()
			{
				Text = AppResources.CounterDetailsDelete
			};
			deleteMenuItem.Click += deleteMenuItem_Click;
			ApplicationBar.MenuItems.Add(deleteMenuItem);

		}

		void deleteMenuItem_Click(object sender, EventArgs e)
		{
			var repository = new CountersRepository();

			repository.DeleteCounter(counter);

			NavigationService.GoBack();
		}

		void cancelButton_Click(object sender, EventArgs e)
		{
			NavigationService.GoBack();
		}

		void saveButton_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(NameText.Text))
			{
				MessageBox.Show(AppResources.CounterDetailsNameRequired);
				return;
			}

			var repository = new CountersRepository();

			int count = 0;
			int.TryParse(CountText.Text, out count);

			counter.Name = NameText.Text;
			counter.Count = count;
			counter.AllowNegatives = AllowNegativesCheckBox.IsChecked.GetValueOrDefault();

			if (counter.ID > 0)
			{
				repository.UpdateCounter(counter);
			}
			else
			{
				counter.CreationDate = DateTime.Now;
				repository.AddNewCounter(counter);
			}

			NavigationService.GoBack();
		}

		private void SelectColorButton_Click(object sender, RoutedEventArgs e)
		{
			NavigationService.Navigate(new Uri("/ColorPickerPage.xaml", UriKind.Relative));
		}
	}
}