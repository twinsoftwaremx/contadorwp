﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ContadorWP.Resources;
using ContadorWP.Model;

namespace ContadorWP
{
	public partial class MainPage : PhoneApplicationPage
	{
		// Constructor
		public MainPage()
		{
			InitializeComponent();

			BuildApplicationBar();
		}

		private void MainPage_Loaded(object sender, RoutedEventArgs e)
		{
			LoadCounters();
		}

		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			base.OnNavigatedTo(e);

			LoadCounters();
		}

		private void BuildApplicationBar()
		{
			ApplicationBar = new ApplicationBar();

			var addCounterButton = new ApplicationBarIconButton()
			{
				IconUri = new Uri("/Assets/Icons/Add-New.png", UriKind.Relative),
				Text = AppResources.MainPageAdd
			};
			addCounterButton.Click += addCounterButton_Click;
			ApplicationBar.Buttons.Add(addCounterButton);

			var aboutMenuItem = new ApplicationBarMenuItem()
			{
				Text = AppResources.MainPageAboutMenuItem
			};
			aboutMenuItem.Click += aboutMenuItem_Click;
			ApplicationBar.MenuItems.Add(aboutMenuItem);
		}

		void aboutMenuItem_Click(object sender, EventArgs e)
		{
			NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.Relative));
		}

		void addCounterButton_Click(object sender, EventArgs e)
		{
			NavigationService.Navigate(new Uri("/CounterDetailsPage.xaml", UriKind.Relative));
		}

		private void EditButton_Click(object sender, RoutedEventArgs e)
		{
			var button = (Button)sender;
			string ID = button.Tag.ToString();

			NavigationService.Navigate(new Uri("/CounterDetailsPage.xaml?ID=" + ID, UriKind.Relative));
		}

		private void DecrementButton_Click(object sender, RoutedEventArgs e)
		{
			var button = (Button)sender;
			var ID = int.Parse(button.Tag.ToString());

			var repository = new CountersRepository();
			var counter = repository.GetCounterByID(ID);
			if (counter != null)
			{
				if (counter.AllowNegatives == false && counter.Count <= 0)
				{
					button.IsEnabled = false;
				}
				else
				{
					counter.Count--;
					repository.UpdateCounter(counter);
				}

			}

			LoadCounters();
		}

		private void IncrementButton_Click(object sender, RoutedEventArgs e)
		{
			var button = (Button)sender;
			var ID = int.Parse(button.Tag.ToString());

			var repository = new CountersRepository();
			var counter = repository.GetCounterByID(ID);
			if (counter != null)
			{
				counter.Count++;
				repository.UpdateCounter(counter);
			}

			LoadCounters();
		}

		private void LoadCounters()
		{
			var repository = new CountersRepository();
			var counters = repository.GetAllCounters();
			CounterListBox.ItemsSource = counters;
		}

		private void IncrementButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
		{
			var button = (Button)sender;
			var ID = int.Parse(button.Tag.ToString());

			var repository = new CountersRepository();
			var counter = repository.GetCounterByID(ID);
			if (counter != null)
			{
				counter.Count++;
				repository.UpdateCounter(counter);
			}

			LoadCounters();
		}

		private void DecrementButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
		{
			var button = (Button)sender;
			var ID = int.Parse(button.Tag.ToString());

			var repository = new CountersRepository();
			var counter = repository.GetCounterByID(ID);
			if (counter != null)
			{
				if (counter.AllowNegatives == false && counter.Count <= 0)
				{
					button.IsEnabled = false;
				}
				else
				{
					counter.Count--;
					repository.UpdateCounter(counter);
				}

			}

			LoadCounters();
		}

	}
}