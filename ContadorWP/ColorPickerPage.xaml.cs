﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ContadorWP.Model;
using System.Windows.Media;

namespace ContadorWP
{
	public partial class ColorPickerPage : PhoneApplicationPage
	{
		public ColorPickerPage()
		{
			InitializeComponent();

			this.Loaded += ColorPickerPage_Loaded;
		}

		private void ColorPickerPage_Loaded(object sender, RoutedEventArgs e)
		{
			List<ColorItem> item = new List<ColorItem>();
			for (int i = 0; i < 66; i++)
			{
				item.Add(new ColorItem() { Text = ColorItem.ColorNames[i], Color = ColorItem.ConvertColor(ColorItem.UintColors[i]) });
			};

			listBox.ItemsSource = item; //Fill ItemSource with all colors
		}

		private void lstColor_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (e.AddedItems.Count > 0)
			{
				(Application.Current as App).CurrentColorItem = ((ColorItem)e.AddedItems[0]);
				this.NavigationService.GoBack();
			}
		}
	}
}