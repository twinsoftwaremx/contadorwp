﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContadorWP.Model
{
	public class Counter
	{
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string Name { get; set; }
		public string Color { get; set; }
		public int Count { get; set; }
		public bool AllowNegatives { get; set; }
		public DateTime CreationDate { get; set; }

		[Ignore()]
		public bool DecrementButtonEnabled
		{
			get
			{
				if (AllowNegatives == false && Count <= 0)
				{
					return false;
				}
				return true;
			}
		}
	}
}
