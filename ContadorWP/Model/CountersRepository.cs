﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace ContadorWP.Model
{
	public class CountersRepository
	{
		public static string DB_PATH = Path.Combine(Path.Combine(ApplicationData.Current.LocalFolder.Path, "contadorwp.sqlite"));

		public void CreateTables()
		{
			var db = GetConnection();
			db.CreateTable<Counter>();
		}

		public List<Counter> GetAllCounters()
		{
			var db = GetConnection();
			var query = db.Query<Counter>("select * from counter");
			return query.ToList<Counter>();
		}

		public void AddNewCounter(Counter counter)
		{
			var db = GetConnection();
			db.Insert(counter);
		}

		public void UpdateCounter(Counter counter)
		{
			var db = GetConnection();
			db.Update(counter);
		}

		public Counter GetCounterByID(int ID)
		{
			var db = GetConnection();
			var counter = db.Get<Counter>(ID);
			return counter;
		}

		public void DeleteCounter(Counter counter)
		{
			var db = GetConnection();
			db.Delete(counter);
		}

		private SQLiteConnection GetConnection()
		{
			var db = new SQLiteConnection(DB_PATH);
			return db;
		}

	}
}
