﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Xml;
using Microsoft.Phone.Tasks;

namespace ContadorWP
{
	public partial class AboutPage : PhoneApplicationPage
	{
		public AboutPage()
		{
			InitializeComponent();
		}

		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			base.OnNavigatedTo(e);

			string appVersion = GetAppVersion();
			AppVersionText.Text = appVersion;
		}

		private string GetAppVersion()
		{
			var xmlReaderSettings = new XmlReaderSettings
			{
				XmlResolver = new XmlXapResolver()
			};

			using (var xmlReader = XmlReader.Create("WMAppManifest.xml", xmlReaderSettings))
			{
				xmlReader.ReadToDescendant("App");

				return xmlReader.GetAttribute("Version");
			}
		}

		private void RateApp_Tap(object sender, System.Windows.Input.GestureEventArgs e)
		{
			var task = new MarketplaceReviewTask();
			task.Show();
		}

		private void ContactSupport_Tap(object sender, System.Windows.Input.GestureEventArgs e)
		{
			var task = new EmailComposeTask();
			task.To = "twinsoftware@hotmail.com";
			task.Subject = "ContadorWP";
			task.Show();
		}
	}
}